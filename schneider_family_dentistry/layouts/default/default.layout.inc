name = Default
description = The Default layout.
template = default-layout

; ========================================
; Regions
; ========================================
regions[usermenu] = User Menu
regions[header] = Header
regions[mainmenu] = Main Menu
regions[help] = Help
regions[highlighted] = Highlighted
regions[contenttop] = Content Top
regions[content] = Content
regions[contentbottom] = Content Bottom
regions[footer] = Footer

; ========================================
; Stylesheets
; ========================================
stylesheets[all][] = css/layouts/default/default.layout.css
stylesheets[all][] = css/layouts/default/default.layout.no-query.css