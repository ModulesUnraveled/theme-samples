<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * Schneider Family Dentistry theme.
 */

/**
 * Replace search button with Font Awesome icon
 */
/*function schneider_family_dentistry_form_search_block_form_alter(&$form, &$form_state, $form_id) {
  $form['actions']['submit']['#value'] = decode_entities('&#xf002;');
  $form['search_block_form']['#attributes']['placeholder'] = t('Search');
}*/

/**
 * Add classes for teaser view mode
 */
/*function schneider_family_dentistry_preprocess_node(&$vars) {
  if($vars['view_mode'] == 'teaser') {
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['node']->type . '__teaser';   
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['node']->nid . '__teaser';
  }
}*/

/**
 * Create custom user-login.tpl.php
 */
/*function schneider_family_dentistry_theme() {
  $items = array();
  $items['user_login'] = array(
  'render element' => 'form',
  'path' => drupal_get_path('theme', 'schneider_family_dentistry') . '/templates',
  'template' => 'user-login',
  'preprocess functions' => array(
  'schneider_family_dentistry_preprocess_user_login'
  ),
 );
return $items;
}*/