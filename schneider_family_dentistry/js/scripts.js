(function($) {

	Drupal.behaviors.menuToggle = {
		attach: function() {
			setTimeout(function() {
				// Set the menu height to full-remaining width
				var menuHeight = $('.l-usermenu').outerHeight();
				menuHeight = (menuHeight === null ? 0 : menuHeight);
				var adminMenuHeight = $('#admin-menu').outerHeight();
				adminMenuHeight = (adminMenuHeight === null ? 0 : adminMenuHeight);
				var headerHeight = $('.l-region--header').outerHeight();
				headerHeight = (headerHeight === null ? 0 : headerHeight);
				var calculatedHeight = (window.innerHeight - menuHeight - adminMenuHeight - headerHeight);
				// Toggle the main menu
				$(".menu-toggle-button").click(function(e){
					e.preventDefault();
					$(".pane-system-main-menu").toggleClass("active");
					$(this).toggleClass("active");
					$(".l-page").toggleClass("active");
					$("body").toggleClass("noscroll");
					$("html").toggleClass("noscroll");
					if ($(".pane-system-main-menu").hasClass("active")) {
						$(".active .menu-wrapper").css('height', calculatedHeight);
						$(".active .menu-wrapper").css('max-height', calculatedHeight);
					} else {
						$(".menu-wrapper").css('max-height', "0px");
					}
				});
			}, 1000);
			// Toggle the second level menus
			$(".menu li.expanded > a").click(function(e){
				e.preventDefault();
				$(this).closest("li").toggleClass("open");
			});

		}
	}

	Drupal.behaviors.fullScreenImage = {
		attach: function() {

			/**
			 * detect IE
			 * returns version of IE or false, if browser is not Internet Explorer
			 */
			function detectIE() {
			    var ua = window.navigator.userAgent;
			
			    var msie = ua.indexOf('MSIE ');
			    if (msie > 0) {
			        // IE 10 or older => return version number
			        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
			    }
			
			    var trident = ua.indexOf('Trident/');
			    if (trident > 0) {
			        // IE 11 => return version number
			        var rv = ua.indexOf('rv:');
			        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
			    }
			
			    var edge = ua.indexOf('Edge/');
			    if (edge > 0) {
			       // IE 12 (aka Edge) => return version number
			       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
			    }
			
			    // other browser
			    return false;
			}

			var IEversion = detectIE();

			if (IEversion !== false) {
			  $(".view-homepage-slider img").css('max-width', "200%");
			}
		}
	}

})(jQuery);