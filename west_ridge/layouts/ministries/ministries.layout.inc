name = Ministries
description = The page layout for Ministries pages.
template = ministries-layout

; ========================================
; Regions
; ========================================
regions[usermenu] = User Menu
regions[header] = Header
regions[mainmenu] = Main Menu
regions[help] = Help
regions[highlighted] = Highlighted
regions[contenttop] = Content Top
regions[content] = Content
regions[sidebar] = Sidebar
regions[contentbottom] = Content Bottom
regions[footer] = Footer

; ========================================
; Stylesheets
; ========================================
stylesheets[all][] = css/layouts/ministries/ministries.layout.css
stylesheets[all][] = css/layouts/ministries/ministries.layout.no-query.css