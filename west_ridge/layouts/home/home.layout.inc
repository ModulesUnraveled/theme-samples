name = Home
description = The layout for Homepage.
template = home-layout

; ========================================
; Regions
; ========================================
regions[usermenu] = User Menu
regions[header] = Header
regions[mainmenu] = Main Menu
regions[help] = Help
regions[highlighted] = Highlighted
regions[contenttop] = Content Top
regions[content] = Content
regions[contentbottom] = Content Bottom
regions[footer] = Footer

; ========================================
; Stylesheets
; ========================================
stylesheets[all][] = css/layouts/home/home.layout.css
stylesheets[all][] = css/layouts/home/home.layout.no-query.css