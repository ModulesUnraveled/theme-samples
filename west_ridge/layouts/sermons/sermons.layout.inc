name = Sermons
description = The Sermons page layout.
template = sermons-layout

; ========================================
; Regions
; ========================================
regions[usermenu] = User Menu
regions[header] = Header
regions[mainmenu] = Main Menu
regions[help] = Help
regions[highlighted] = Highlighted
regions[contenttop] = Content Top
regions[content] = Content
regions[sidebar] = Sidebar
regions[contentbottom] = Content Bottom
regions[footer] = Footer

; ========================================
; Stylesheets
; ========================================
stylesheets[all][] = css/layouts/sermons/sermons.layout.css
stylesheets[all][] = css/layouts/sermons/sermons.layout.no-query.css