name = Calendar
description = The layout for Calendar pages.
template = calendar-layout

; ========================================
; Regions
; ========================================
regions[usermenu] = User Menu
regions[header] = Header
regions[mainmenu] = Main Menu
regions[help] = Help
regions[highlighted] = Highlighted
regions[contenttop] = Content Top
regions[content] = Content
regions[contentbottom] = Content Bottom
regions[footer] = Footer

; ========================================
; Stylesheets
; ========================================
stylesheets[all][] = css/layouts/calendar/calendar.layout.css
stylesheets[all][] = css/layouts/calendar/calendar.layout.no-query.css