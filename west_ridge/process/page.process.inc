<?php

/**
 * Implements hook_process_page().
 */
function west_ridge_process_page(&$variables) {
  // You can use process hooks to modify the variables before they are passed to
  // the theme function or template file.
}
