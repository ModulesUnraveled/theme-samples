# What is this?

This is a repo containing a few sample themes that I have created. They were all created using Omega 4.x in Drupal 7. They also all used Panels everywhere with custom layouts (in the theme folders.)

## West Ridge Community Church
[_https://www.westridgecc.org_](https://www.westridgecc.org/)

### Items of note:

* **Custom Javascript mobile menu (12 lines of jQuery)**
    * west_ridge/js/scripts.js ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/west_ridge/js/scripts.js))
    * west_ridge/templates/panels-pane--block--system-main-menu.tpl.php ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/west_ridge/templates/panels-pane--block--system-main-menu.tpl.php))
    * west_ridge/sass/components/menus/_menu-main.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/west_ridge/sass/components/menus/_menu-main.scss))
* **CSS only desktop dropdown menu**
    * west_ridge/sass/components/menus/_menu-main.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/west_ridge/sass/components/menus/_menu-main.scss))
* **User-friendly search box that starts small, then expands when active to allow more room for text. (CSS only. No JavaScript!)**
    * west_ridge/sass/components/_search.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/west_ridge/sass/components/_search.scss))
    * west_ridge/template.php ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/west_ridge/template.php))

## Schneider Family Dentistry
[_http://schneiderdental.net_](http://schneiderdental.net/)

### Items of note:

* **Custom, full-screen, Javascript mobile menu**
    * schneider_family_dentistry/js/scripts.js ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/schneider_family_dentistry/js/scripts.js))
    * schneider_family_dentistry/sass/components/menus/_menu-main-a.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/schneider_family_dentistry/sass/components/menus/_menu-main-a.scss))
    * schneider_family_dentistry/sass/components/menus/_menu-main-toggle.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/schneider_family_dentistry/sass/components/menus/_menu-main-toggle.scss))
* **Full-width, responsive, homepage slider built with Views Slideshow, Picture and Breakpoints modules**
    * schneider_family_dentistry/sass/layouts/home/components/_homepage-slider.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/schneider_family_dentistry/sass/layouts/home/components/_homepage-slider.scss))
* **Responsive [request appointment](http://schneiderdental.net/request-an-appointment) and [contact](http://schneiderdental.net/contact) forms built with the entityform module**
    * schneider_family_dentistry/sass/abstractions/_button.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/schneider_family_dentistry/sass/abstractions/_button.scss))
    * schneider_family_dentistry/sass/base/_forms.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/schneider_family_dentistry/sass/base/_forms.scss))
    * schneider_family_dentistry/sass/components/forms/_contact-form.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/schneider_family_dentistry/sass/components/forms/_contact-form.scss))
    * schneider_family_dentistry/sass/components/forms/_request-appointment-form.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/schneider_family_dentistry/sass/components/forms/_request-appointment-form.scss))

## Filimin
_I no longer manage this site. The site owner has taken over development. I'll be replacing this code sample in the future, but until then, you can still see the site I built at the following URL._
[_http://pfilim.vps-2820.devpanel.net_](http://pfilim.vps-2820.devpanel.net/)

**Items of note:**

* **The way Paragraphs (from the Paragraphs module) are styled on the [support page](http://pfilim.vps-2820.devpanel.net/support). Especially the responsive table (that's not really a table) ie: status codes**
    * filimin/sass/components/paragraphs/_paragraphs.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/filimin/sass/components/paragraphs/_paragraphs.scss))
    * filimin/sass/components/paragraphs/_paragraphs-containers.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/filimin/sass/components/paragraphs/_paragraphs-containers.scss))
    * filimin/sass/components/paragraphs/_paragraphs-headings.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/filimin/sass/components/paragraphs/_paragraphs-headings.scss))
    * filimin/sass/components/paragraphs/_paragraphs-status-codes.scss ([view code](https://bitbucket.org/ModulesUnraveled/theme-samples/src/c3d4c0249e125e33a29b8ce061de5520db09297a/filimin/sass/components/paragraphs/_paragraphs-status-codes.scss))