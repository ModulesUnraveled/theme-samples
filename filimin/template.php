<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * West Ridge theme.
 */

function filimin_form_search_block_form_alter(&$form, &$form_state, $form_id) {
  $form['actions']['submit']['#value'] = decode_entities('&#xf002;');
  $form['search_block_form']['#attributes']['placeholder'] = t('Search');
}