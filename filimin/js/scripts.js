(function($) {
	$(document).ready(function() {
		$(".mobile-menu-toggle").click(function(e){
			e.preventDefault();
			var $menuContainer = $(".menu-wrapper"),
					menuState = $menuContainer.attr("data-menu-state"),
					newMenuState = (menuState == "open") ? "closed" : "open";

					$menuContainer.attr("data-menu-state", newMenuState);
		});
	});
})(jQuery);