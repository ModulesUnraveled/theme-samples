<div class="l-page">
  <?php if (!empty($page['usermenu'])): ?>
    <div class="l-usermenu">
      <?php print render($page['usermenu']); ?>
    </div>
  <?php endif; ?>

  <div class="l-mainmenu">
    <?php print render($page['mainmenu']); ?>
  </div>
  
  <header class="l-header" role="banner">
    <?php print render($page['header']); ?>
  </header>

  <?php if (!empty($page['highlighted'])): ?>
    <div class="l-highlighted">
      <?php print render($page['highlighted']); ?>
    </div>
  <?php endif; ?>

  <div class="l-main">
    <div class="l-content" role="main">
      <?php if (!empty($page['contenttop'])): ?>
        <div class="l-contenttop">
          <?php print render($page['contenttop']); ?>
        </div>
      <?php endif; ?>

      <?php if (!empty($page['help'])): ?>
        <div class="l-help">
          <?php print render($page['help']); ?>
        </div>
      <?php endif; ?>

      <div class="l-body">
        <a id="main-content"></a>
        <div class="l-maincontent">
          <?php print render($page['content']); ?>
        </div>
        <div class="l-sidebar">
          <?php print render($page['sidebar']); ?>
        </div>
      </div>
      
      <?php if (!empty($page['contentbottom'])): ?>
        <div class="l-contentbottom">
          <?php print render($page['contentbottom']); ?>
        </div>
      <?php endif; ?>

    </div>
  </div>

  <?php if (!empty($page['footer'])): ?>
    <footer class="l-footer" role="contentinfo">
      <?php print render($page['footer']); ?>
    </footer>
  <?php endif; ?>
</div>
